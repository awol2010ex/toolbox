import React from "react";
import 'element-theme-default';
import {  Layout,Button } from 'element-react';
import "../css/QrEncode.css"
//import {CreateQRCode}  from "../api/qrcode"
//QR码输出
class QrEncode extends React.Component {
    async componentDidMount() {
   
    }

    //监控变化
    componentWillUpdate(nextProps) {
    }
    constructor(props){
        super(props);

        this.state={
            qrcode:"https://www.baidu.com",
            url:""
        }
    }
    //生成QR码
    doQRCodeCreate(){
        this.refs.createQrcodeForm.submit();
    }
    //Qrcode双向绑定
    handleQrcode(e){
        this.setState({
            qrcode:e.target.value
        })
    }
    //渲染
    render() {
        return (
            <Layout.Row>
                <Layout.Col span="5" style={{ "padding": "20px" }}>
                      <Layout.Col span="24">
                      URL或其他文本:
                      </Layout.Col>
                      <Layout.Col span="24">
                       <form method="post" ref="createQrcodeForm" target="imageIframe" action="/api/qrcode/create">   
                      <textarea name="qrcode" style={{ "width": "200px","height": "400px" }} value={this.state.qrcode} onChange={(e) => this.handleQrcode(e)}></textarea>
                       </form>
                      </Layout.Col>
                </Layout.Col>
                <Layout.Col span="2" style={{ "padding": "20px" }}>
                      <Layout.Col span="24">
                         <Button type="primary"  onClick={() => this.doQRCodeCreate()}>生成QR码</Button>
                      </Layout.Col>
                </Layout.Col>
                <Layout.Col span="5" style={{ "padding": "20px" }}>
                      <Layout.Col span="24">
                      QR码：
                      </Layout.Col>
                      <Layout.Col span="24">
                        <iframe name="imageIframe" title="imageIframe" frameBorder={0} style={{"width":"500px","height":"500px"}}></iframe>
                      </Layout.Col>
                </Layout.Col>
            </Layout.Row>
            
        )
    }
}
export default QrEncode