import React from "react";
import { BrowserRouter, Switch, Route ,Link} from "react-router-dom";

import 'element-theme-default';
import { Layout, Menu } from 'element-react';


import QrEncode from "./components/QrEncode"//QR码输出
import Home from "./components/Home"//主页面
class Routers extends React.Component {

    render() {
        return (

            //路由home页面
            <BrowserRouter>

                <Layout.Row>
                    <Layout.Col span="24">
                        <Menu theme="dark" defaultActive="1" mode="horizontal" >
                            <Menu.Item index="0"><Link to={ '/' }>首页</Link></Menu.Item>
                            <Menu.Item index="1">格式化</Menu.Item>
                            <Menu.SubMenu index="2" title="加密/转码">
                                <Menu.Item index="2-1"><Link to={ '/qr/encode' }>生成QR码</Link></Menu.Item>
                                <Menu.Item index="2-2">解析QR码</Menu.Item>
                            </Menu.SubMenu>
                            <Menu.Item index="3">其他</Menu.Item>
                        </Menu>
                    </Layout.Col>
                </Layout.Row>
                <Layout.Row>
                    <Layout.Col span="24">

                        <Switch>
                            <Route
                                exact
                                path="/"
                                render={props => (
                                    <Home {...props} />
                                )}
                            />
                            <Route
                                exact
                                path="/qr/encode"
                                render={props => (
                                    <QrEncode {...props} />
                                )}
                            />
                        </Switch>
                    </Layout.Col>
                </Layout.Row>


            </BrowserRouter>
        )
    }
}
export default Routers