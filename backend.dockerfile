FROM alpine:latest
MAINTAINER devplatform
VOLUME /tmp
COPY toolbox /opt
RUN chmod 777 /opt/toolbox
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo "Asia/Shanghai" > /etc/timezone
WORKDIR /opt
ENTRYPOINT [ "./toolbox" ]
EXPOSE 9999
