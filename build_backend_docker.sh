GOOS=linux GOARCH=amd64 go build -tags netgo --ldflags "-extldflags -static" -o toolbox
docker build --no-cache  -f backend.dockerfile -t toolbox/toolbox-backend .
docker tag toolbox/toolbox-backend 10.110.36.168:5000/toolbox/toolbox-backend
docker push 10.110.36.168:5000/toolbox/toolbox-backend