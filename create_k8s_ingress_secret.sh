#文件名不能修改,否则会造成ingress 访问404
openssl req -newkey rsa:2048 -nodes -keyout /opt/k8s/tls.key -x509 -days 365 -out /opt/k8s/tls.crt
kubectl delete secret  toolbox-tls-cert
kubectl create secret generic toolbox-tls-cert --from-file=/opt/k8s/tls.key --from-file=/opt/k8s/tls.crt