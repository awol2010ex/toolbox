FROM nginx:alpine
MAINTAINER devplatform
VOLUME /tmp
COPY front-end/mime.types /opt
COPY front-end/build /opt/build
COPY front-end/cert /opt/cert
RUN mkdir -p /opt/logs
RUN mkdir -p /opt/temp
RUN mkdir -p /etc/nginx/logs
COPY front-end/front-end-prod-nginx.conf /opt
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo "Asia/Shanghai" > /etc/timezone

WORKDIR /opt
EXPOSE 30051
EXPOSE 30151
CMD ["nginx","-c","/opt/front-end-prod-nginx.conf" ,"-g", "daemon off;"]
