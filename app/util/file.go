package util

import (
	"os"
)

// 创建文件夹
func CreateDir(Path string) {

	if _, err := os.Stat(Path); os.IsNotExist(err) {
		// 必须分成两步：先创建文件夹、再修改权限
		os.MkdirAll(Path, 0777) //0777也可以os.ModePerm
		os.Chmod(Path, 0777)
	}
}
