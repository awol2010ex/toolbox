package exception

import (
	"runtime/debug"
	"strings"
	"toolbox/app/util"

	"github.com/gin-gonic/gin"
)

func SetUp() gin.HandlerFunc {

	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {

				DebugStack := ""
				for _, v := range strings.Split(string(debug.Stack()), "\n") {
					DebugStack += v + "<br>"
				}

				utilGin := util.Gin{Ctx: c}
				utilGin.Response(500, "系统异常，请联系管理员！", DebugStack)
			}
		}()
		c.Next()
	}
}
