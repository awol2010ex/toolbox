package route

import (
	"toolbox/app/route/middleware/exception"
	"toolbox/app/route/middleware/logger"
	"toolbox/app/util"

	"github.com/gin-gonic/gin"
	"toolbox/app/controller/qrcode"
)
/*
SetupRouter 设置路由
*/
func SetupRouter(engine *gin.Engine) {

	//设置路由中间件
	engine.Use(logger.SetUp(), exception.SetUp())

	//404
	engine.NoRoute(func(c *gin.Context) {
		utilGin := util.Gin{Ctx: c}
		utilGin.Response(404, "请求方法不存在", nil)
	})

	engine.GET("/ping", func(c *gin.Context) {
		utilGin := util.Gin{Ctx: c}
		utilGin.Response(1, "pong", nil)
	})
	
    /*设置QRcode路由*/
    qrcode.SetupRouters(engine)
}
