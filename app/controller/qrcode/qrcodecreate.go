package qrcode

import (
	"github.com/gin-gonic/gin"
	"toolbox/app/util"
)
import qrcode "github.com/skip2/go-qrcode"

/*
CreateQRCode 输出QRcode
*/
func CreateQRCode(c *gin.Context) {
	utilGin := util.Gin{Ctx: c}
	c.Writer.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	c.Writer.Header().Set("Pragma", "no-cache")
	c.Writer.Header().Set("Expires", "0")
	c.Writer.Header().Set("Content-Type", "image/png")

	/*Qrcode*/
	qrcodeStr ,_:= c.GetPostForm("qrcode")
	println("qrcodeStr=" + qrcodeStr)
	var png []byte
	png, err := qrcode.Encode(qrcodeStr, qrcode.Medium, 256)
	if err != nil {
		utilGin.Response(500, "系统异常，请联系管理员！", nil)
	} else {
		c.Writer.Write(png)
	}

}

/*
SetupRouters 设置路由
*/
func SetupRouters(engine *gin.Engine) {
	//CreateQRCode 输出QRcode
	engine.POST("/qrcode/create", CreateQRCode)
}
