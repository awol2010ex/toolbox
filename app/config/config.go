package config

import "toolbox/app/util"

func init() {
	util.CreateDir(AppLogPath)
}

const (
	AppMode = "release" //debug or release
	AppPort = ":9999"
	AppName = "toolbox"

	// 超时时间
	AppReadTimeout  = 120
	AppWriteTimeout = 120

	AppLogPath = "log"

	// 日志文件
	AppAccessLogName = AppLogPath + "/" + AppName + "-access.log"
	AppErrorLogName  = AppLogPath + "/" + AppName + "-error.log"
)
