cd front-end
git config --global url."https://".insteadOf git://
npm config set registry http://registry.npm.taobao.org/
npm config set sass_binary_site=https://npm.taobao.org/mirrors/node-sass/
npm install --unsafe-perm
npm run build
cd ..
docker build --no-cache  -f frontend.dockerfile -t toolbox/toolbox-frontend .
docker tag toolbox/toolbox-frontend 10.110.36.168:5000/toolbox/toolbox-frontend
docker push 10.110.36.168:5000/toolbox/toolbox-frontend
